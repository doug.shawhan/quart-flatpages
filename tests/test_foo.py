from contextlib import contextmanager
import os
from pathlib import Path
import pytest
import pytest_asyncio

from paka import cmark
from quart import Quart, render_template_string

from quart_flatpages import FlatPages

from . import renderer, temp_directory


@contextmanager
def temp_pages(app=None, name=None):

    with temp_directory() as temp:
        # nope. pass it something useful
        source = Path(os.path.dirname(__file__), "instance")

        app = app or Quart(__name__)

        if name is None:
            app.config.update({"FLATPAGES_ROOT": "repositories/faux-repo"})

        yield FlatPages(app, name)


@pytest.mark.asyncio
async def test_foo():
    quapp = Quart(__name__)
    quapp.config.update({
        "FLATPAGES_FAUXREPO_ROOT": "repositories/faux-repo",
        "FLATPAGES_FAUXREPO_EXTENSION": [".md"],
        "FLATPAGES_FAUXREPO_INSTANCE_RELATIVE": True,
        "FLATPAGES_FAUXREPO_REQUIRED_METADATA": ("author", "title", "published"),
    })
    with temp_pages(quapp, "FAUXREPO") as pages:
        assert pages.get("Faux Repo/foo").__html__ == """<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n"""




