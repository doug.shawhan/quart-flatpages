from contextlib import contextmanager
import json
from pathlib import Path
import pytest
import pytest_asyncio

from quart import Quart, render_template_string
from quart_flatpages import FlatPages

repo_name = "FAUXREPO"
app = Quart(__name__, instance_relative_config=True)
flatpages = FlatPages(name=repo_name)

CONFIG = {
    "FLATPAGES_FAUXREPO_ROOT": "repositories/faux-repo",
    "FLATPAGES_FAUXREPO_EXTENSION": [".md"],
    "FLATPAGES_FAUXREPO_INSTANCE_RELATIVE": True,
}

class QuartApp: # pylint: disable=too-few-public-methods
    """
    Fakey quart app
    """

    def __init__(self, config_file=None):
        """
        placeholder
        """
        self.config_file = config_file

    def init_app(self):
        """
        Generate app for tests.
        """

        app.debug = True
        # TODO: use app.open_instance_resource to load a cfg file
        # from instance folder
        app.config.update(CONFIG)
        flatpages.init_app(app)

        return app
        
@app.route("/")
async def index():
    """
    base route
    """
    return await render_template_string("Hello")


@pytest_asyncio.fixture
def quart_app(request):
    """
    Create a fixture
    """
    return QuartApp(request.param)


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", ("config",), indirect=True)
async def test_base(quart_app):
    """
    simple test
    """
    response = await quart_app.init_app().test_client().get("/")
    assert await response.data == b"Hello"


@pytest.mark.asyncio
@pytest.mark.parametrize("quart_app", ("config",), indirect=True)
async def test_document_root(quart_app):
    """
    Does document root return the proper path?
    """
    quap = quart_app.init_app()
    full_path = Path(Path().absolute(), "instance/repositories/faux-repo")
    assert quap.extensions["flatpages"][repo_name].root == full_path
