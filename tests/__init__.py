from contextlib import contextmanager
import shutil
import tempfile

from paka import cmark

def renderer(obj):
    return cmark.to_html(obj.body)


@contextmanager
def temp_directory():
    directory = tempfile.mkdtemp()
    try:
        yield directory
    finally:
        shutil.rmtree(directory)
