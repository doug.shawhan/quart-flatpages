"""
Simple page object
"""

import datetime
from dataclasses import dataclass, field
import pathlib
from typing import Any, Union, List

@dataclass(order=True)
class Page:  # pylint: disable=too-many-instance-attributes
    """
    page object
    """

    path: pathlib.Path
    meta: dict = field(repr=False)
    body: str = field(repr=False)  # markdown processed to html
    folder: pathlib.Path = field(repr=False)
    modified: float = field(repr=False)

    def __post_init__(self):
        """
        do thing
        """
        self.kind = self.__class__.__name__.lower()
        self.__html__ = self.body


    def __getitem__(self, name):
        """
        Shortcut to metadata dictionary.

        ``page['title']`` or, in a template, ``{{ page.title }}`` are
        equivalent to ``page.meta['title']``.
        """
        return self.meta.get(name)
