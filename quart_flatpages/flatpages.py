"""
Flatpages extension for Quart
"""

from datetime import datetime
"""
Load Flatpages
"""
from io import StringIO

import os
import pathlib
from pathlib import Path

from paka import cmark
from quart import Quart, abort, current_app
from werkzeug.utils import cached_property
from typing import Sequence, Generator, Tuple, Any
import yaml

from .page import Page


class FlatPages:
    """
    FlatPage loader.
    """

    DEFAULTS = (
        ("root", "pages"),
        ("extensions", ".md"),
        ("encoding", "utf-8"),
        ("html_renderer", cmark.to_html),
        ("instance_relative", False),
    )

    def __init__(self, app=None, name=None) -> None:
        """
        Set configuration, cache and pages object

        Args:
            app (flask.Flask): flask app
            name (str): Name of documents repository.
        """

        self.name = name

        if self.name is None:
            self.config_prefix = "FLATPAGES"
        else:
            self.config_prefix = f"FLATPAGES_{self.name.upper()}"

        self._file_cache = {}

        if app is not None:
            self.init_app(app)

        self.__pages = {}


    def __iter__(self):
        "Assure one can iterate on page objects."
        return iter(self.pages)

    def init_app(self, app: Quart) -> None:
        """
        Initialize ``Quart`` application
        """

        # Store default config to application
        for key, value in self.DEFAULTS:
            config_key = "_".join((self.config_prefix, key.upper()))
            app.config.setdefault(config_key, value)

        # make the context magic happen by storing the application to
        # the current instance and the current instance to the application.
        if "flatpages" not in app.extensions:
            app.extensions["flatpages"] = {}
        app.extensions["flatpages"][self.name] = self

        self.app = app


    def config(self, key) -> Any:
        """
        Translate flatpages-specific config to use Name

        Args:
            key (str): short version of ``FLATPAGES_`` configuration entries.

        Returns:
            configuration element
        """
        return self.app.config[f"{self.config_prefix}_{key.upper()}"]

    def get(self, path, default=None):
        """
        Get html page.

        Args:
            path (str): path portion of url
            default (str): default error message

        Returns:
            page (str): html page
        """
        try:
            return self.pages[path]
        except KeyError:
            return default

    def get_or_404(self, path):
        """
        Return the :class:`Page` object at ``path``.
        Raise 404 error if there is no such page.
        """
        page = self.get(path)
        if not page:
            abort(404)
        return page

    @cached_property
    def root(self):
        """
        Root of documents repository.
        """
        if self.config("instance_relative"):
            return Path(self.app.instance_path, self.config("root"))
        return Path(self.app.root_path, self.config("root"))

    async def async_load(self):
        """
        Pre-load pages.
        """
        self._load_pages()


    async def hard_reload(self):
        """
        forget all pages
        """
        self.__pages = {}


    async def reload(self):
        """
        only load new or modified pages
        """
        self._load_pages(refresh=True)


    def _load_pages(self, refresh=False):
        """
        Page loader
        """

        extensions = self.config("extensions")

        if not hasattr(extensions, "__iter__"):
            raise TypeError(
                "'extensions' value is {type(extensions).__name__}. "
                "Expected tuple of strings."
            )

        if isinstance(extensions, str):
            extensions = (self.config("extensions"),)
        else:
            extensions = tuple(self.config("extensions"))

        for path, full_path, folder in _md_walker(self.root, extensions):
            if not refresh:
                if path in self.__pages:
                    raise ValueError(
                        f"Multiple markdown documents found for {path}. "
                        "This error can arise when using muliple extensions."
                    )
                self.__pages[path] = self._load_file(path, full_path, folder)
            else:
                self.__pages[path] = self._refresh_file(path, full_path, folder)


    @property
    def pages(self, refresh=False) -> dict:
        """
        Returns: pages (dict): dictionary of pages keyed by path
        """

        if not self.__pages:
            self._load_pages()
        return self.__pages


    def _refresh_file(self, path, full_path, folder):
        """
        refresh individual file
        """
        page = self._file_cache.get(str(full_path))

        # No updates! Return that page.
        if page and page.modified == datetime.fromtimestamp(full_path.stat().st_mtime):
            return page
        else:
            self._load_file(path, full_path, folder)


    def _load_file(self, path, full_path, folder):
        """
        load individual file
        """

        with open(full_path, "r", encoding=self.config("encoding")) as _fh:

            raw_meta, raw_content = _fh.read().split(f"{os.linesep}{os.linesep}", 1)
            meta = self._meta(raw_meta)
            body = self.config("html_renderer")(raw_content)

            page = Page(
                path,
                meta,
                body,
                folder,
                datetime.fromtimestamp(full_path.stat().st_mtime),
            )
            self._file_cache[str(full_path)] = page
            return page


    def _meta(self, raw_meta):
        """
        Get metadata
        """

        _meta = {}

        # _obj is generator
        for line in raw_meta.split(os.linesep):
            val = yaml.safe_load(line)
            if not isinstance(val, dict):
                break
            _meta.update(val)

        # check for required metadata fields, expect tuple
        required = self.config("required_metadata")
        if required:
            found = set(required).intersection(set(_meta.keys()))
            if not found == set(required):
                raise ValueError(f"Expected metadata: {self.required_metadata_fields}")

        return _meta


def _md_walker(root_dir: pathlib.Path, extensions: Sequence[str]) -> Generator[Tuple[str, pathlib.Path, pathlib.Path], None, None]:
    """
    Walk document directory
    Find pages with markdown by extension defined in
    ``FLATPAGES_{repo}_EXTENSION``

    Yields:
        path (str): ``path`` portion of url. Acts as key
        full_path (pathlib.Path): cannonical path to markdown file.
        folder (pathlib.Path): Path relative to instance.
    """

    for base, _, filenames in os.walk(root_dir):
        folder = str(Path(base)).replace(str(root_dir), "").lstrip(os.sep)
        path_prefix = tuple(folder.split(os.sep)) if folder else ()
        for filename in filenames:
            if filename.endswith(extensions):
                full_path = Path(base, filename)
                name_without_extension = full_path.stem

                path = "/".join(path_prefix + (full_path.stem,))
                full_path = Path(base, filename)

                yield path, full_path, Path(folder)
